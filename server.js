'use strict';

const fs = require('fs');
const express = require('express');

let g = 0;

function initialCall() {
    fs.writeFile('./test.txt', g, (err) => {
        if (err) throw err;
    })
}

initialCall();

// Constants
const PORT = 8080;
const HOST = '0.0.0.0';

// App
const app = express();
app.get('/', (req, res) => {
  res.send('Hello world\n');
});

app.listen(PORT, HOST);
console.log(`Running on http://${HOST}:${PORT}`);


function appendCall() {
    fs.appendFile('./test.txt', g);
    g += 1;

    console.log(g);
}

setInterval(appendCall, 1000);